﻿using AutoMapper;
using ListatTestTask_DAL.Models;
using ListatTestTask_DAL.Repositories.Interfaces;
using ListatTestTask_Models.EntityModels.Auction.DTO;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListatTestTask_DAL.Repositories.Repositories
{
    public class AuctionRepository : BaseRepository<Auction, AuctionDTO>, IAuctionRepository
    {
        public AuctionRepository(ApplicationContext context, IMapper mapper, IMemoryCache memoryCache) : base(context, mapper, memoryCache)
        {
        }
    }
}