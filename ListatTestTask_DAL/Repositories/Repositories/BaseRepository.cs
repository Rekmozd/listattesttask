﻿using AutoMapper;
using ListatTestTask_DAL.Models;
using ListatTestTask_DAL.Repositories.Interfaces;
using ListatTestTask_Models.EntityModels.Base.DTO;
using ListatTestTask_Models.InternalModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ListatTestTask_DAL.Repositories.Repositories
{
    public class BaseRepository<TEntity, TEntityDTO> : IBaseRepository<TEntity, TEntityDTO>
        where TEntity : BaseEntity
        where TEntityDTO : BaseDTO
    {
        internal readonly DbSet<TEntity> _dbSet;
        internal readonly ApplicationContext _context;
        internal readonly IMapper _mapper;
        private readonly IMemoryCache _memoryCache;
        public BaseRepository(ApplicationContext context, 
            IMapper mapper, 
            IMemoryCache memoryCache)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
            _mapper = mapper;
            _memoryCache = memoryCache;
        }

        private IQueryable<TEntity> Include(Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeProperties)
        {
            IQueryable<TEntity> query = _dbSet.AsNoTracking();
            return includeProperties(query);
        }
        public async Task<(RequestResult, IEnumerable<TEntityDTO>)> GetAll<TKey>(Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include, bool desc, Func<TEntity, TKey> sortingBy, int page, int count)
        {
            try
            {
                IEnumerable<TEntity> result;
                var skip = page * count;
                if (desc)
                    result = Include(include).OrderByDescending(sortingBy).Skip(skip).Take(count).ToList();
                else
                    result = Include(include).OrderBy(sortingBy).Skip(skip).Take(count).ToList();
                return (RequestResult.Success(), await Task.Run(() => _mapper.Map<IEnumerable<TEntityDTO>>(result)));
            }
            catch (Exception ex)
            {
                return (RequestResult.Failure(this, ex.Message), Enumerable.Empty<TEntityDTO>());
            }
        }
        public async Task<(RequestResult, IEnumerable<TEntityDTO>)> GetByFilter<TKey>(Expression<Func<TEntity, bool>> predicate, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include, bool desc, Func<TEntity, TKey> sortingBy, int page, int count)
        {
            try
            {                
                IEnumerable<TEntity> result;
                var skip = page * count;
                if (desc)
                    result = Include(include).Where(predicate).OrderByDescending(sortingBy).Skip(skip).Take(count).ToList();
                else
                    result = Include(include).Where(predicate).OrderBy(sortingBy).Skip(skip).Take(count).ToList();
                return (RequestResult.Success(), await Task.Run(() => _mapper.Map<IEnumerable<TEntityDTO>>(result)));
            }
            catch (Exception ex)
            {
                return (RequestResult.Failure(this, ex.Message), Enumerable.Empty<TEntityDTO>());
            }
        }
        public async Task<(RequestResult, TEntityDTO?)> GetById(int id, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include)
        {
            if (_memoryCache.TryGetValue(id, out TEntityDTO? entity))
                return (RequestResult.Success(), entity);
            try
            {
                var result = await Include(include).FirstOrDefaultAsync(x => x.Id == id);
                var entityDTO = _mapper.Map<TEntityDTO>(result);
                _memoryCache.Set(id, entityDTO, TimeSpan.FromMinutes(5));
                return (RequestResult.Success(), entityDTO);
            }
            catch (Exception ex)
            {
                return (RequestResult.Failure(this, ex.Message), null);
            }
        }
        public async Task<(RequestResult, TEntityDTO?)> GetById(int id)
        {
            if (_memoryCache.TryGetValue(id, out TEntityDTO? entity))
                return (RequestResult.Success(), entity);
            try
            {
                var result = await _dbSet.FirstOrDefaultAsync(x => x.Id == id);
                var entityDTO = _mapper.Map<TEntityDTO>(result);
                _memoryCache.Set(id, entityDTO, TimeSpan.FromMinutes(5));
                return (RequestResult.Success(), entityDTO);
            }
            catch (Exception ex)
            {
                return (RequestResult.Failure(this, ex.Message), null);
            }
        }
    }
}