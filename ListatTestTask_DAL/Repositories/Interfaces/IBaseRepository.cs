﻿using ListatTestTask_DAL.Models;
using ListatTestTask_Models.EntityModels.Base.DTO;
using ListatTestTask_Models.InternalModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ListatTestTask_DAL.Repositories.Interfaces
{
    public interface IBaseRepository<TEntity, TEntityDTO> 
        where TEntityDTO : BaseDTO
        where TEntity : BaseEntity
    {
        Task<(RequestResult, TEntityDTO?)> GetById(int id);
        Task<(RequestResult, TEntityDTO?)> GetById(int id, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include);
        Task<(RequestResult, IEnumerable<TEntityDTO>)> GetAll<TKey>(Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include, bool desc, Func<TEntity, TKey> sortingBy, int page, int count);
        Task<(RequestResult, IEnumerable<TEntityDTO>)> GetByFilter<TKey>(Expression<Func<TEntity, bool>> predicate, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include, bool desc, Func<TEntity, TKey> sortingBy, int page, int count);
    }
}