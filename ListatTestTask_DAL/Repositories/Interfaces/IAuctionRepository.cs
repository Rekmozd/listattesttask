﻿using ListatTestTask_DAL.Models;
using ListatTestTask_Models.EntityModels.Auction.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListatTestTask_DAL.Repositories.Interfaces
{
    public interface IAuctionRepository : IBaseRepository<Auction, AuctionDTO>
    {
    }
}
