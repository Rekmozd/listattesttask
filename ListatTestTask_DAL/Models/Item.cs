﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListatTestTask_DAL.Models
{
    [Index("NormalizedName", IsUnique = true)]
    public class Item : BaseEntity
    {
        public required string Name { get; set; }
        public required string NormalizedName { get { return NormalizedName; } set { _ = Name.ToUpper(); } }
        public required string Description { get; set; }
        public required string Metadata { get; set; }
        public virtual ICollection<Auction> Auctions { get; set; } = new List<Auction>();
    }
}