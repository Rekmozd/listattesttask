﻿using ListatTestTask_Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListatTestTask_DAL.Models
{
    public class Auction : BaseEntity
    {
        public required Item Item { get; set; }
        public DateTime CreateDt { get; set; }
        public DateTime FinishedDt { get; set;}
        public double Price { get; set; }
        public required string Status { get; set; }
        public required string Seller { get; set; }
        public required string Buyer { get; set; }
    }
}