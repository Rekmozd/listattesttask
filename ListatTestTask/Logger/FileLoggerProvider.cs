﻿namespace ListatTestTask.Logger
{
    public class FileLoggerProvider : ILoggerProvider
    {
        private readonly string path;
        public FileLoggerProvider(string path)
        {
            this.path = path;
        }
        public ILogger CreateLogger(string categoryName)
        {
            return FileLogger.GetInstance(path);
        }
        public void Dispose() { }
    }
}