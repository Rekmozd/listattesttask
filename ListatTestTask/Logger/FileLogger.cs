﻿using System.Text;

namespace ListatTestTask.Logger
{
    public class FileLogger : ILogger, IDisposable
    {
        private readonly string filePath;
        private static FileLogger instance;
        private object _lock = new object();
        private FileLogger(string path)
        {
            filePath = path;
        }

        public static FileLogger GetInstance(string path)
        {
            instance ??= new FileLogger(path);
            return instance;
        }

        public IDisposable? BeginScope<TState>(TState state) where TState : notnull
        {
            return this;
        }

        public void Dispose() { }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception? exception, Func<TState, Exception?, string> formatter)
        {
            StringBuilder sb = new ( $"LogTime: {DateTime.UtcNow} \nLogLevel: {logLevel} \nEventId: {eventId}\n State: {state}\n");
            if (exception != null) sb
                    .Append($"Exception message: {exception.Message}\n")
                    .Append(formatter(state, exception))
                    .Append("\n");
            lock (_lock)
            {
                File.AppendAllText(filePath, sb.ToString());
            }
        }
    }
}