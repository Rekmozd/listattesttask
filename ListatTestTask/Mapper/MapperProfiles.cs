﻿using AutoMapper;
using ListatTestTask_DAL.Models;
using ListatTestTask_Models.EntityModels.Auction.DTO;
using ListatTestTask_Models.EntityModels.Auction.View;
using ListatTestTask_Models.EntityModels.Item.DTO;
using ListatTestTask_Models.EntityModels.Item.View;

namespace ListatTestTask.Mapper
{
    public class MapperProfiles : Profile
    {
        public MapperProfiles()
        {
            #region Item
            CreateMap<Item, ItemDTO>();

            CreateMap<ItemDTO, ItemView>();
            #endregion
            #region Auction
            CreateMap<Auction, AuctionDTO>();
            CreateMap<AuctionDTO, AuctionView>();
            #endregion
        }
    }
}
