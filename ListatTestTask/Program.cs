using AutoMapper;
using ListatTestTask.Logger;
using ListatTestTask.Mapper;
using ListatTestTask_BLL.Services.Interfaces;
using ListatTestTask_BLL.Services.Services;
using ListatTestTask_DAL;
using ListatTestTask_DAL.Repositories.Interfaces;
using ListatTestTask_DAL.Repositories.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;
var _configuration = builder.Configuration;
#region Caching
services.AddMemoryCache();
#endregion
#region Logger
builder.Logging.AddFile(Path.Combine(Directory.GetCurrentDirectory(), "Log.txt"));
#endregion
#region Database
services.AddDbContext<ApplicationContext>(options =>
{
    options.UseSqlServer(_configuration.GetConnectionString("DatabaseConnection"));
});
#endregion
#region Repositories
services.AddScoped(typeof(IBaseRepository<,>), typeof(BaseRepository<,>));
services.AddScoped<IAuctionRepository, AuctionRepository>();
#endregion
#region Services
services.AddScoped(typeof(IBaseService<,>), typeof(BaseService<,>));
services.AddScoped<IAuctionService, AuctionService>();
#endregion
#region AutoMapper
var mapConfig = new MapperConfiguration(cfg =>
{
    cfg.AddProfile(new MapperProfiles());
});
var mapper = mapConfig.CreateMapper();
services.AddSingleton(mapper);
#endregion
services.AddControllers()
    .AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
    });
services.AddEndpointsApiExplorer();
#region Swagger
services.AddSwaggerGen(options =>
{
    var title = "ListatTestTask";
    var description = "Http client for ListatTestTask";
    options.SwaggerDoc("v0.1", new OpenApiInfo
    {
        Title = title,
        Description = description,
        Version = "v0.1"
    });
    options.SwaggerDoc("v0.2", new OpenApiInfo
    {
        Title = title,
        Description = description,
        Version = "v0.2"
    });
});
#endregion
var app = builder.Build();
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        options.SwaggerEndpoint("/swagger/v0.1/swagger.json", "v0.1");
        options.SwaggerEndpoint("/swagger/v0.2/swagger.json", "v0.2");
    });
}
app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.Run();