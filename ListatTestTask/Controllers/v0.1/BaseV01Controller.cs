﻿using Microsoft.AspNetCore.Mvc;

namespace ListatTestTask.Controllers.v0._1
{
    [ApiController]
    [Route("v0.1/[controller]")]
    [ApiExplorerSettings(GroupName = "v0.1")]
    public class BaseV01Controller : ControllerBase
    {
        
    }
}
