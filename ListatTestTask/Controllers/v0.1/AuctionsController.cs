﻿using Microsoft.EntityFrameworkCore;
using ListatTestTask.Controllers.v0._1;
using Microsoft.AspNetCore.Mvc;
using ListatTestTask_Models.Enums;
using ListatTestTask_Models.EntityModels.Auction.View;
using ListatTestTask_DAL.Repositories.Interfaces;
using AutoMapper;
using ListatTestTask_DAL.Models;
using System.Linq.Expressions;
using ListatTestTask_Models.InternalModels;
using ListatTestTask_BLL.Services.Interfaces;
using ListatTestTask_BLL.Models;

namespace ListatTestTask.Controllers
{
    public class AuctionsController : BaseV01Controller
    {
        private readonly IAuctionService _auctionService;
        private readonly IMapper _mapper;
        public AuctionsController(IAuctionService auctionRepository,
            IMapper mapper)
        {
            _auctionService = auctionRepository;
            _mapper = mapper;
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<AuctionView>> GetById(int id)
        {
            var result = await _auctionService.GetById(id);
            if (!result.Item1.Succeed) return BadRequest(result.Item1);
            var mappedResult = _mapper.Map<AuctionView>(result.Item2);
            return Ok(mappedResult);
        }
        [HttpGet]
        public async Task<ActionResult<ICollection<AuctionView>>> GetBy(OrderBy orderBy, SortingBy sortingBy, AuctionStatus status, string? seller, string? name, int page = 0, int pageLimit = 10)
        {
            var result = await _auctionService.Get(new FilterModel(orderBy, sortingBy, status, seller, name, page, pageLimit));
            if (!result.Item1.Succeed) return BadRequest(result.Item1);
            var mappedResult = _mapper.Map<IEnumerable<AuctionView>>(result.Item2);
            return Ok(mappedResult);
        }        
    }
}