﻿using Microsoft.AspNetCore.Mvc;

namespace ListatTestTask.Controllers.v0._2
{
    [ApiController]
    [Route("v0.2/[controller]")]
    [ApiExplorerSettings(GroupName = "v0.2")]
    public class Basev02Controller : ControllerBase
    {
       
    }
}