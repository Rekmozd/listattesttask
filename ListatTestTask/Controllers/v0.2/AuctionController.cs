﻿using Microsoft.AspNetCore.Mvc;

namespace ListatTestTask.Controllers.v0._2
{
    public class AuctionController : Basev02Controller
    {
        [HttpGet]
        public IActionResult TestV02GetRequest()
        {
            return Ok();
        }
    }
}