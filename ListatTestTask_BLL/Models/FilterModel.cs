﻿using ListatTestTask_Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListatTestTask_BLL.Models
{
    public record FilterModel(OrderBy OrderBy, SortingBy SortingBy, AuctionStatus Status, string? Seller, string? Name, int Page, int PageLimit);
    
}