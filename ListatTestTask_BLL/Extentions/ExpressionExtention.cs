﻿using ListatTestTask_DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ListatTestTask_BLL.Extentions
{
    public static class ExpressionExtention
    {
        public static Expression<Func<Auction, bool>> AddFilteringCondition<T>(this Expression<Func<T, bool>> leftExpression, Expression<Func<T, bool>> rightExpression)
        {
            var param = rightExpression.Parameters.ToArray();
            if (leftExpression != null)
            {
                var body = Expression.AndAlso(Expression.Invoke(leftExpression, param), Expression.Invoke(rightExpression, param));
                return Expression.Lambda<Func<Auction, bool>>(body, param);
            }
            return Expression.Lambda<Func<Auction, bool>>(rightExpression.Body, param);
        }
    }
}