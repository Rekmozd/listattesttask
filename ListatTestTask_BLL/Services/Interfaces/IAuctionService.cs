﻿using ListatTestTask_BLL.Models;
using ListatTestTask_DAL.Models;
using ListatTestTask_Models.EntityModels.Auction.DTO;
using ListatTestTask_Models.InternalModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListatTestTask_BLL.Services.Interfaces
{
    public interface IAuctionService : IBaseService<Auction, AuctionDTO>
    {
        Task<(RequestResult, IEnumerable<AuctionDTO>)> Get(FilterModel filter);            
    }
}