﻿using ListatTestTask_BLL.Services.Interfaces;
using ListatTestTask_DAL.Models;
using ListatTestTask_DAL.Repositories.Interfaces;
using ListatTestTask_Models.EntityModels.Base.DTO;
using ListatTestTask_Models.InternalModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListatTestTask_BLL.Services.Services
{
    public class BaseService<TEntity, TEntityDTO> : IBaseService<TEntity, TEntityDTO>
        where TEntity : BaseEntity
        where TEntityDTO : BaseDTO
    {
        private readonly IBaseRepository<TEntity, TEntityDTO> _repository;
        public BaseService(IBaseRepository<TEntity, TEntityDTO> repository)
        {
            _repository = repository;
        }

        public virtual async Task<(RequestResult, TEntityDTO?)> GetById(int id)
        {
            return await _repository.GetById(id);
        }
    }
}