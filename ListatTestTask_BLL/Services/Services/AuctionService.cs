﻿using Azure;
using ListatTestTask_BLL.Extentions;
using ListatTestTask_BLL.Models;
using ListatTestTask_BLL.Services.Interfaces;
using ListatTestTask_DAL.Models;
using ListatTestTask_DAL.Repositories.Interfaces;
using ListatTestTask_Models.EntityModels.Auction.DTO;
using ListatTestTask_Models.Enums;
using ListatTestTask_Models.InternalModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ListatTestTask_BLL.Services.Services
{
    public class AuctionService : BaseService<Auction, AuctionDTO>, IBaseService<Auction, AuctionDTO>, IAuctionService
    {
        private readonly IAuctionRepository _auctionRepository;
        public AuctionService(IAuctionRepository auctionRepository) : base(auctionRepository)
        {
            _auctionRepository = auctionRepository;
        }

        public override async Task<(RequestResult, AuctionDTO?)> GetById(int id)
        {
            return await _auctionRepository.GetById(id, x => x.Include(i => i.Item));
        }
        public async Task<(RequestResult, IEnumerable<AuctionDTO>)> Get(FilterModel filter)
        {
            var filterExpression = FilterBuilder(filter);
            var sortingFunc = GetSortFunc(filter.SortingBy);
            if (filterExpression == null) return await _auctionRepository.GetAll(x => x.Include(i => i.Item), filter.OrderBy == OrderBy.desc, sortingFunc, filter.Page, filter.PageLimit);
            var result = await _auctionRepository.GetByFilter(
                filterExpression!,
                x => x
                .Include(i => i.Item),
                filter.OrderBy == OrderBy.desc,
                sortingFunc,
                filter.Page,
                filter.PageLimit);
            return result;
        }


        private Func<Auction, object?> GetSortFunc(SortingBy SortingBy)
        {
            return (x) =>
            {
                object? result = SortingBy switch
                {
                    SortingBy.Price => x.Price,
                    SortingBy.CreatedDt => x.CreateDt,
                    _ => null,
                };
                return result;
            };
        }
        private Expression<Func<Auction, bool>>? FilterBuilder(FilterModel filter)
        {
            Expression<Func<Auction, bool>>? filteringExpression = null;
            if (filter.Name != null)
            {
                Expression<Func<Auction, bool>> newExpression = x => x.Item.NormalizedName == filter.Name.ToUpper();
                filteringExpression = filteringExpression!.AddFilteringCondition(newExpression);
            }

            if (filter.Seller != null)
            {
                Expression<Func<Auction, bool>> newExpression = x => x.Seller == filter.Seller;
                filteringExpression = filteringExpression!.AddFilteringCondition(newExpression);
            }

            if (filter.Status != AuctionStatus.None)
            {
                Expression<Func<Auction, bool>> newExpression = (x) => x.Status == filter.Status.ToString();
                filteringExpression = filteringExpression!.AddFilteringCondition(newExpression);
            }
            return filteringExpression;
        }
    }
}