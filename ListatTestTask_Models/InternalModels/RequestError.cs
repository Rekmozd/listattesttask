﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListatTestTask_Models.InternalModels
{
    public sealed class RequestError
    {
        public string Place { get; }
        public string Message { get; }
        public RequestError(object place, string message)
        {
            Place = place.GetType().Name;
            Message = message;
        }
    }
}
