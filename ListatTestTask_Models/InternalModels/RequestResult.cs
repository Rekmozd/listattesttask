﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListatTestTask_Models.InternalModels
{
    public sealed class RequestResult
    {
        private RequestResult()
        {
            Errors = Enumerable.Empty<RequestError>();
        }
        private RequestResult(IEnumerable<RequestError> errors)
        {
            Errors = errors;
        }
        public IEnumerable<RequestError> Errors { get; }
        public bool Succeed => !Errors.Any();
        public static RequestResult Success() => new();
        public static RequestResult Failure<T>(T calledClass, string message)
            where T : class
        {
            return new RequestResult(new[] { new RequestError(calledClass, message) });
        }
        public static RequestResult Failure(IEnumerable<RequestError> errors)
        {
            return new RequestResult(errors);
        }
    }
}
