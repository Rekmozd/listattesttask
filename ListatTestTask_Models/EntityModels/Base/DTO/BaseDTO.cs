﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListatTestTask_Models.EntityModels.Base.DTO
{
    public class BaseDTO
    {
        public int Id { get; set; }
    }
}