﻿using ListatTestTask_Models.EntityModels.Base.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListatTestTask_Models.EntityModels.Item.DTO
{
    public class ItemDTO : BaseDTO
    {        
        public required string Name { get; set; }
        public required string Description { get; set; }
        public required string Metadata { get; set; }
    }
}