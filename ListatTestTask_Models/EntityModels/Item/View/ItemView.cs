﻿using ListatTestTask_Models.EntityModels.Base.DTO;
using ListatTestTask_Models.EntityModels.Base.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListatTestTask_Models.EntityModels.Item.View
{
    public class ItemView : BaseView
    {
        public required string Name { get; set; }
        public required string Description { get; set; }
        public required string Metadat { get; set; }
    }
}