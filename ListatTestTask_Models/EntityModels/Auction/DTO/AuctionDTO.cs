﻿using ListatTestTask_Models.EntityModels.Base.DTO;
using ListatTestTask_Models.EntityModels.Item.DTO;
using ListatTestTask_Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListatTestTask_Models.EntityModels.Auction.DTO
{
    public class AuctionDTO : BaseDTO
    {        
        public required ItemDTO Item { get; set; }
        public DateTime CreatedDt { get; set; }
        public DateTime FinishedDt { get; set; }
        public double Price { get; set; }
        public AuctionStatus Status { get; set; }
        public required string Seller { get; set; }
        public required string Buyer { get; set; }
    }
}